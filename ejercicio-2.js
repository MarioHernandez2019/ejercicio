{
  const {
    html,
  } = Polymer;
  /**
    `<ejercicio-1>` Description.

    Example:

    ```html
    <ejercicio-1></ejercicio-1>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --ejercicio-1 | :host    | {} |

    * @customElement ejercicio-1
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class Ejercicio2 extends Polymer.Element {

    static get is() {
      return 'ejercicio-2';
    }

    static get properties() {
      return {};
    }

    static get template() {
      return html `
      <style include="ejercicio-2-styles ejercicio-2-shared-styles"></style>
      <slot></slot>
      
      Nombre:
      <cells-molecule-input 
        label="Nombre" 
        type="text"> 
      </cells-molecule-input>
      Apellido Paterno:
      <cells-molecule-input 
          label="Apellido Paterno" 
          type="text"> 
      </cells-molecule-input>
    Apellido Materno:
    <cells-molecule-input 
          label="Apellido Materno" 
          type="text"> 
      </cells-molecule-input>
      Telefono:
      <cells-molecule-input 
          label="Telefono" 
          type="text"> 
      </cells-molecule-input>
      Correo:
      <cells-molecule-input 
          label="Correo" 
          type="text"> 
      </cells-molecule-input>
      Apodo:
      <cells-molecule-input 
          label="Apodo" 
          type="text"> 
      </cells-molecule-input> 
      `;
    }

callToback(e){
  console.log('hello');

}

  }

  customElements.define(Ejercicio2.is, Ejercicio2);
}