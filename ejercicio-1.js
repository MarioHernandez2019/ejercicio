{
  const {
    html,
  } = Polymer;
  /**
    `<ejercicio-1>` Description.

    Example:

    ```html
    <ejercicio-1></ejercicio-1>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --ejercicio-1 | :host    | {} |

    * @customElement ejercicio-1
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class Ejercicio1 extends Polymer.Element {

    static get is() {
      return 'ejercicio-1';
    }

    static get properties() {
      return {};
    }

    static get template() {
      return html `
      <style include="ejercicio-1-styles ejercicio-1-shared-styles"></style>
      <slot></slot>
      
      Correo:
      <cells-molecule-input 
          label="Correo" 
          type="text"> 
      </cells-molecule-input>

      Mensaje:
      <cells-molecule-input 
          label="Mensaje" 
          type="text"> 
      </cells-molecule-input> 


     <cells-st-button on-click="callToback">
        <button>
        Enviar
        </button>
        </cells-st-button>
      
      `;
    }

callToback(e){
  console.log('hello');

}

  }

  customElements.define(Ejercicio1.is, Ejercicio1);
}